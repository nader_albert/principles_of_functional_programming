package forcomp

import scala.annotation.tailrec

object Anagrams {

  /** A word is simply a `String`. */
  type Word = String

  /** A sentence is a `List` of words. */
  type Sentence = List[Word]

  /** `Occurrences` is a `List` of pairs of characters and positive integers saying
   *  how often the character appears.
   *  This list is sorted alphabetically w.r.t. to the character in each pair.
   *  All characters in the occurrence list are lowercase.
   *  
   *  Any list of pairs of lowercase characters and their frequency which is not sorted
   *  is **not** an occurrence list.
   *  
   *  Note: If the frequency of some character is zero, then that character should not be
   *  in the list.
   */
  type Occurrences = List[(Char, Int)]

  /** The dictionary is simply a sequence of words.
   *  It is predefined and obtained as a sequence using the utility method `loadDictionary`.
   */
  val dictionary: List[Word] = loadDictionary

  /** Converts the word into its character occurrence list.
   *  
   *  Note: the uppercase and lowercase version of the character are treated as the
   *  same character, and are represented as a lowercase character in the occurrence list.
   *
   *  The groupBy method takes a function mapping an element of a collection to a key of some other type, and produces
   *  a Map of keys and collections of elements which mapped to the same key
   */
  def wordOccurrences(w: Word): Occurrences = w.groupBy(elem => elem.toLower).map(tuple => (tuple._1,tuple._2.length))
    .toList.sortWith((elem1,elem2) => elem1._1 < elem2._1)
    //val list = w.groupBy(elem => w.count(p => p == elem)).toList

  /** Converts a sentence into its character occurrence list. */
  def sentenceOccurrences(s: Sentence): Occurrences = wordOccurrences(s.foldLeft("":Word)((acc,word) => acc + word ))

  /** The `dictionaryByOccurrences` is a `Map` from different occurrences to a sequence of all
   *  the words that have that occurrence count.
   *  This map serves as an easy way to obtain all the anagrams of a word given its occurrence list.
   *  
   *  For example, the word "eat" has the following character occurrence list:
   *
   *     `List(('a', 1), ('e', 1), ('t', 1))`
   *
   *  Incidentally, so do the words "ate" and "tea".
   *
   *  This means that the `dictionaryByOccurrences` map will contain an entry:
   *
   *    List(('a', 1), ('e', 1), ('t', 1)) -> Seq("ate", "eat", "tea")
   *
   */
  lazy val dictionaryByOccurrences: Map[Occurrences, List[Word]] = dictionary.groupBy(word => wordOccurrences(word))

  /** Returns all the anagrams of a given word. */
  def wordAnagrams(word: Word): List[Word] =
    dictionaryByOccurrences.get(wordOccurrences(word)) match {
      case Some(words) => words
      case _ => List()
    }

  def combinations(occurrences: Occurrences): List[Occurrences]= {
    /**
     * @param occurrence, ex: (a,2)
     * @return a list of n occurrences, where each occurrences is another list of occurrence
     *         ex: List(
     *                  List ((a,1)),
     *                  List ((a,2))
     *             )
     */
    def variate(occurrence: (Char, Int)): IndexedSeq[(Char, Int)] =
      for (alternativeFreqs <- 1 to occurrence._2)
        yield (occurrence._1, alternativeFreqs)

    /**
     * */
     def combine(occurrence: (Char, Int), occurrencesList: List[Occurrences]): List[Occurrences] =
      occurrencesList match {
        case Nil =>
          variate(occurrence).
            foldLeft(occurrencesList) ((acc,occurrenceVariation) => List(occurrenceVariation) :: acc)
        case head::Nil =>
          variate(occurrence).
            foldLeft(occurrencesList) ((acc,occurrenceVariation) => (List(occurrenceVariation) ::: head) :: acc)
        case _::_ =>
          variate(occurrence)
            .foldLeft(occurrencesList) ((acc,occurrenceVariation) => occurrencesList
              .foldLeft(acc)((acc,elem) => (List(occurrenceVariation) ::: elem) :: acc))
      }

    occurrences match {
      case x::Nil => combine(x,List(Nil))
      case x::xs => combine(x,combinations(xs))
      case _ => throw new Error ("a case that should never happen")
    }
  }

  /** Returns the list of all subsets of the occurrence list.
   *  This includes the occurrence itself, i.e. `List(('k', 1), ('o', 1))`
   *  is a subset of `List(('k', 1), ('o', 1))`.
   *  It also include the empty subset `List()`.
   * 
   *  Example: the subsets of the occurrence list `List(('a', 2), ('b', 2))` are:
   *
   *    List(
   *      List(),
   *      List(('a', 1)),
   *      List(('a', 2)),
   *      List(('b', 1)),
   *      List(('a', 1), ('b', 1)),
   *      List(('a', 2), ('b', 1)),
   *      List(('b', 2)),
   *      List(('a', 1), ('b', 2)),
   *      List(('a', 2), ('b', 2))
   *    )
   *
   *  Note that the order of the occurrence list subsets does not matter -- the subsets
   *  in the example above could have been displayed in some other order.
    *
    * each call to this method, picks one element of the Occurrences and computes all its combinations with the
    * remaining elements
   */
  /*def combinations(occurrences: Occurrences): List[Occurrences]= {

    def computeVariations(occurrences: Occurrences): List[Occurrences]=
        occurrences match {
          case x :: xs =>
            val alternativeOccurrences =
              for (alternativeFreqs <- 1 to x._2)
                yield (x._1, alternativeFreqs)

            alternativeOccurrences.toList :: computeVariations(xs)
          case Nil => List()
        } //each occurrences element in the list is one of the variations of a single character i.e. List((a,1),(a,2),(a,3))

    val occurrenceVariations = computeVariations(occurrences)

    def combineOccurrences(firstOccurrences: Occurrences, secondOccurrences: Occurrences): List[Occurrences]= {
      val i =
        for (firstOccurrence <- firstOccurrences; secondOccurrence <- secondOccurrences)
        yield List(firstOccurrence, secondOccurrence)

      i
    }

    /***
      * the passed ist of occurrences already hold the single cases
      * List(
      *   List((a,1),(a,2)),
      *   List((b,1),(b,2)),
      *   List((c,1),(c,2)),
      *   )
      * */
    //@tailrec
    def combineOccurrencesList(firstOccurrencesList: List[Occurrences], secondOccurrencesList: List[Occurrences]): List[Occurrences]= {
      secondOccurrencesList match {
        case Nil => firstOccurrencesList
        case x::Nil => val y = firstOccurrencesList.foldLeft(List[Occurrences]())((acc,elem) => acc ::: combineOccurrences(x,elem))
                       y
        case x::xs =>
          val k = firstOccurrencesList.foldLeft(List[Occurrences]())((acc1,elem1) => acc1 ::: secondOccurrencesList.foldLeft(acc1)((acc2,elem2) => acc2 ::: combineOccurrences(elem1,elem2)))
          val x = combineOccurrencesList(k, xs)
          x
      }

      /*occurrencesList match {
        case Nil => Nil
        case x::Nil => combineOccurrences(x,secondOccurrencesList.head)
        //case x :: xs if occurrenceVariations.length == stopCondition /*what should this number be*/ => occurrencesVariations
        case x::xs => println("combine" + x + "&&" + xs.head)//combine(x, xs.head) ::: combineOccurrences(xs.tail)
          //combineOccurrences(combine(x,xs.head) ::: xs.tail)
          combineOccurrences(x,xs.head) ::: combineOccurrencesList(firstOccurrencesList.tail,secondOccurrencesList.tail)
      }*/
    }

    combineOccurrencesList(List(occurrenceVariations.head)/*List(a1, a2, a3)*/, occurrenceVariations.tail /*List (List (b1,b2)), List((c1,c2))*/)
  }*/

  /** Subtracts occurrence list `y` from occurrence list `x`.
   * 
   *  The precondition is that the occurrence list `y` is a subset of
   *  the occurrence list `x` -- any character appearing in `y` must
   *  appear in `x`, and its frequency in `y` must be smaller or equal
   *  than its frequency in `x`.
   *
   *  Note: the resulting value is an occurrence - meaning it is sorted
   *  and has no zero-entries.
   */
  def subtract(x: Occurrences, y: Occurrences): Occurrences = {
    val originalMap: Map[Char, Occurrences] = x.groupBy(p => p._1).withDefault(char => List((char,0)))

    val subtractedMap: Map[Char,Occurrences] =
      y.foldLeft(originalMap)((acc,occurrence) => acc.apply(occurrence._1) match {
        case Nil => throw new Error ("shall never happen since the map provides a default")
        case head::Nil =>
          if (head._2 == 0)
           throw new Error("occurrences to be subtracted are not satisfying the precondition of being smaller than the base")
          else {
            if (head._2 == 1)
              (acc - head._1).updated(head._1,List((head._1,head._2 - occurrence._2)))
            else
              acc.updated(head._1,List((head._1,head._2 - occurrence._2)))
          }
        case _::_ => throw new Error("invalid occurrences given. duplicate occurrences are detected")
      })
    subtractedMap.foldLeft(List(): Occurrences)((acc1,p)=> p._2 ::: acc1).sortWith((elem1,elem2) => elem1._1 < elem2._1)
  }

  /** Returns a list of all anagram sentences of the given sentence.
   *  
   *  An anagram of a sentence is formed by taking the occurrences of all the characters of
   *  all the words in the sentence, and producing all possible combinations of words with those characters,
   *  such that the words have to be from the dictionary.
   *
   *  The number of words in the sentence and its anagrams does not have to correspond.
   *  For example, the sentence `List("I", "love", "you")` is an anagram of the sentence `List("You", "olive")`.
   *
   *  Also, two sentences with the same words but in a different order are considered two different anagrams.
   *  For example, sentences `List("You", "olive")` and `List("olive", "you")` are different anagrams of
   *  `List("I", "love", "you")`.
   *  
   *  Here is a full example of a sentence `List("Yes", "man")` and its anagrams for our dictionary:
   *
   *  The list of occurrences for this sentence would be:
   *   List(
   *      List(),
   *      List(('y',1)),
   *      List(('y',1), ('e',1)),
   *      List(('y',1), ('a',1), (s,1)),  //say
   *      List(('m',1), ('e',1), (n,1)),  //men
   *      List(('m',1), ('a',1), (s,1)),  //no words in dictionary
   *      List(('y',1), ('e',1), (s,1)),  //yes
   *      List(('y',1), (e,1), (s,1), (m,1)), // -> nothing found in dictionary
   *      List(('e',1), ('n',1), (s,1), (a,1)), sean and sane -> two words found
   *      List(('y',1), ('e',1), (s,1), (m,1), (a,1)),
   *
   *    List(
   *      List(en, as, my),
   *      List(en, my, as),
   *      List(man, yes),
   *      List(men, say),
   *      List(as, en, my),
   *      List(as, my, en),
   *      List(sane, my),
   *      List(Sean, my),
   *      List(my, en, as),
   *      List(my, as, en),
   *      List(my, sane),
   *      List(my, Sean),
   *      List(say, men),
   *      List(yes, man)
   *    )
   *
   *  The different sentences do not have to be output in the order shown above - any order is fine as long as
   *  all the anagrams are there. Every returned word has to exist in the dictionary.
   *  
   *  Note: in case that the words of the sentence are in the dictionary, then the sentence is the anagram of itself,
   *  so it has to be returned in this list.
   *
   *  Note: There is only one anagram of an empty sentence.
   *
   */
  def sentenceAnagrams(sentence: Sentence): List[Sentence] = ??? //{
    /** the recursive solution */

    /*val occurrences = sentenceOccurrences(sentence)
    val possibleCombinations = combinations(occurrences)

    def combinationAnagram(possibleCombinations: List[Occurrences], sentences:List[Sentence]): List[Sentence] = {
      //base case: the occurrence list of the produced sentence is exactly equal to the occurrence list of the original sentence?!
      //if (sentenceOccurrences(sentence) == )
      //
      dictionaryByOccurrences.get(currentPossibleCombination) match {
        case Some(words) => //addWordsToSentences(words)
          words.map(List(_))

          /*@tailrec
          def addWordsToSentences(words: List[Word]): List[Sentence] = {
            sentences.map(words.head :: _)
            addWordsToSentences(words.tail)
          }*/

      }
      combinationAnagram(subtract(possibleCombinations, currentPossibleCombination))
    }

    combinationAnagram(combinations(sentenceOccurrences(sentence)),List())// empty list of words, representing an accumulator
    */
//we should inspect the whole list of combinations for each single sentence

    // 1- compute the occurrence list of the given sentence
    // 2- get all possible combinations from this occurrence list
    // 3- for each possible combination,
    //    3.1. pick all words with similar occurrence from the dictionary
    //    3.2. each word retrieved will represent a word in a separate sentence
    //    3.3. the combination in hand should be subtracted from the list of possible combinations, for each sentence in hand
    // 4- a solution (sentence) is considered as complete (valid anagram of the original sentence), if its occurrence
    //    list matches the occurrence list of the original sentence


    /** the for comprehension solution */
    /*for(combination <- combinations(sentenceOccurrences(sentence)) if dictionaryByOccurrences.contains(combination))
      yield dictionaryByOccurrences.get(combination).getOrElse(List())*/
  //}

}
