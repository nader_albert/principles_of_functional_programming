package objsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TweetSetSuite extends FunSuite {
  trait TestSets {
    val set1 = new Empty
    val set2 = set1.incl(new Tweet("a", "a body", 20))
    val set3 = set2.incl(new Tweet("b", "b body", 20))
    val c = new Tweet("c", "c body", 7)
    val d = new Tweet("d", "d body", 9)
    val e = new Tweet("e", "e body", 19)
    val f = new Tweet("f", "f body", 15)
    val g = new Tweet("g", "g body", 55)
    val h = new Tweet("h", "h body", 64)

    val set4c = set3.incl(c)
    val set4d = set3.incl(d)
    val set5 = set4c.incl(d)
    val set6 = set5.incl(g)

    val set7 = set6.incl(f).incl(g).incl(h)
  }

  def asSet(tweets: TweetSet): Set[Tweet] = {
    var res = Set[Tweet]()
    tweets.foreach(res += _)
    res
  }

  def size(set: TweetSet): Int = asSet(set).size

  test("filter: on empty set") {
    new TestSets {
      assert(size(set1.filter(tw => tw.user == "a")) === 0)
    }
  }

  test("filter: on set 3 and elem a") {
    new TestSets {
      assert(size(set3.filter(tw => tw.user == "a")) === 1)
    }
  }

  test("filter: on set 3 and elem b") {
    new TestSets {
      assert(size(set3.filter(tw => tw.user == "b")) === 1)
    }
  }

  test("filter: on set 3 and elem a and b") {
    new TestSets {
      assert(size(set3.filter(tw => tw.user == "a" || tw.user == "b")) === 2)
    }
  }

  test("filter: a on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.user == "a")) === 1)
    }
  }

  test("filter: a,b,c,and d with set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.user == "a" || tw.user == "b" || tw.user == "c" || tw.user == "d" )) === 4)
    }
  }

  test("filter: a,b, c and d with set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.user == "a" || tw.user == "b" || tw.user == "c" && tw.user == "d" )) === 2)
    }
  }

  test("filter: (a,b, c) and d with set5") {
    new TestSets {
      assert(size(set5.filter(tw => (tw.user == "a" || tw.user == "b" || tw.user == "c") && tw.user == "d" )) === 0)
    }
  }

  test("filter: 20 on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.retweets == 20)) === 2)
    }
  }

  test("filter: <=20 on set4c") {
    new TestSets {
      assert(size(set4c.filter(tw => tw.retweets <= 20)) === 3)
    }
  }

  test("filter: <=21 on set4c") {
    new TestSets {
      assert(size(set2.filter(tw => tw.retweets <= 21)) === 1)
    }
  }

  test("filter: <20 on set4c") {
    new TestSets {
      assert(size(set4c.filter(tw => tw.retweets < 20)) === 1)
    }
  }

  test("filter: 9 on set4c") {
    new TestSets {
      assert(size(set4d.filter(tw => tw.retweets == 9)) === 1)
    }
  }

  test("filter: 7 on set4c") {
    new TestSets {
      assert(size(set4c.filter(tw => tw.retweets == 7)) === 1)
    }
  }

  test("filter: <20 on set4d") {
    new TestSets {
      assert(size(set4d.filter(tw => tw.retweets < 20)) === 1)
    }
  }

  test("filter: bodies on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.text.contains("body"))) === 4)
    }
  }

  test("filter: nody on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.text.contains("nody"))) === 0)
    }
  }

  /** ***************** union tests ******************** */
  test("union: set4c and set4d") {
    new TestSets {
      assert(size(set4c.union(set4d)) === 4)
    }
  }

  test("union: with empty set (1)") {
    new TestSets {
      assert(size(set5.union(set1)) === 4)
    }
  }

  test("union: with empty set (2)") {
    new TestSets {
      assert(size(set1.union(set5)) === 4)
    }
  }

  test("union: set5 with set5") {
    new TestSets {
      assert(size(set5.union(set5)) === 4)
    }
  }

  test("union: set2 with set3") {
    new TestSets {
      assert(size(set2.union(set3)) === 2)
    }
  }

  /** ***************** descending tests ******************** */
  test("descending: set5") {
    new TestSets {
      val trends = set5.descendingByRetweet
      assert(!trends.isEmpty)
      assert(trends.head.user == "a" || trends.head.user == "b")
    }
  }

  test("descending: set4d") {
    new TestSets {
      val trends = set4d.descendingByRetweet
      assert(!trends.isEmpty)
      assert(trends.head.user == "a" || trends.head.user == "b")
    }
  }

  test("descending: set4c") {
    new TestSets {
      val trends = set4d.descendingByRetweet
      assert(!trends.isEmpty)
      assert(trends.head.user == "a" || trends.head.user == "b")
    }
  }

  test("descending: set6") {
    new TestSets {
      val trends = set6.descendingByRetweet
      assert(!trends.isEmpty)
      assert(trends.head.retweets === 55 )//&& trends.head.user == "e")
    }
  }

  test("descending: set6 print") {
    new TestSets {
      val trends = set6.descendingByRetweet
      assert(!trends.isEmpty)
      trends.foreach(println)
    }
  }

  test("descending: set7 print") {
    new TestSets {
     val trends = set7.descendingByRetweet
     assert(!trends.isEmpty)
     assert(set7.mostRetweeted.retweets === 64)
     trends.foreach(println)
    }
  }
}
