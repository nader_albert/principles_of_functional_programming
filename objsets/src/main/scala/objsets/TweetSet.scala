package objsets

import common._
import TweetReader._
import java.util.NoSuchElementException
import scala.NoSuchElementException

/**
 * A class to represent tweets.
 */
class Tweet(val user: String, val text: String, val retweets: Int) {
  override def toString: String =
    "User: " + user + "\n" +
    "Text: " + text + " [" + retweets + "]"
}

/**
 * This represents a set of objects of type `Tweet` in the form of a binary search
 * tree. Every branch in the tree has two children (two `TweetSet`s). There is an
 * invariant which always holds: for every branch `b`, all elements in the left
 * subtree are smaller than the tweet at `b`. The eleemnts in the right subtree are
 * larger.
 *
 * Note that the above structure requires us to be able to compare two tweets (we
 * need to be able to say which of two tweets is larger, or if they are equal). In
 * this implementation, the equality / order of tweets is based on the tweet's text
 * (see `def incl`). Hence, a `TweetSet` could not contain two tweets with the same
 * text from different users.
 *
 *
 * The advantage of representing sets as binary search trees is that the elements
 * of the set can be found quickly. If you want to learn more you can take a look
 * at the Wikipedia page [1], but this is not necessary in order to solve this
 * assignment.
 *
 * [1] http://en.wikipedia.org/wiki/Binary_search_tree
 */
abstract class TweetSet {

  /**
   * This method takes a predicate and returns a subset of all the elements
   * in the original set for which the predicate is true.
   *
   * Question: Can we implement this method here, or should it remain abstract
   * and be implemented in the subclasses?
   */
  def filter(p: Tweet => Boolean): TweetSet = filterAcc(p,new Empty)

  /**
   * This is a helper method for `filter` that propagates the accumulated tweets.
   */
  def filterAcc(p: Tweet => Boolean, acc: TweetSet): TweetSet

  /**
   * Returns a new `TweetSet` that is the union of `TweetSet`s `this` and `that`.
   *
   * Question: Should we implement this method here, or should it remain abstract
   * and be implemented in the subclasses?
   */
  def union(that: TweetSet): TweetSet = ???

  /**
   * Returns the tweet from this set which has the greatest retweet count.
   *
   * Calling `mostRetweeted` on an empty set should throw an exception of
   * type `java.util.NoSuchElementException`.
   *
   * Question: Should we implement this method here, or should it remain abstract
   * and be implemented in the subclasses?
   */
  def mostRetweeted: Tweet = ???

  /** this is mine */
  def influential(thatTweet: Tweet) = thatTweet

  /**
   * Returns a list containing all tweets of this set, sorted by retweet count
   * in descending order. In other words, the head of the resulting list should
   * have the highest retweet count.
   *
   * Hint: the method `remove` on TweetSet will be very useful.
   * Question: Should we implement this method here, or should it remain abstract
   * and be implemented in the subclasses?
   */
  def descendingByRetweet: TweetList = ???

  /**
   * The following methods are already implemented
   */

  /**
   * Returns a new `TweetSet` which contains all elements of this set, and the
   * the new element `tweet` in case it does not already exist in this set.
   *
   * If `this.contains(tweet)`, the current set is returned.
   */
  def incl(tweet: Tweet): TweetSet

  /**
   * Returns a new `TweetSet` which excludes `tweet`.
   */
  def remove(tweet: Tweet): TweetSet

  /**
   * Tests if `tweet` exists in this `TweetSet`.
   */
  def contains(tweet: Tweet): Boolean

  /**
   * This method takes a function and applies it to every element in the set.
   */
  def foreach(f: Tweet => Unit): Unit
}

class Empty extends TweetSet {

  override def filterAcc(p: Tweet => Boolean, acc: TweetSet): TweetSet = acc

  override def union(that: TweetSet): TweetSet = that //this empty list has nothing to share with the given 'that' set

  override def descendingByRetweet:TweetList = Nil

  override def mostRetweeted: Tweet = throw new NoSuchElementException

  override def influential(thatTweet: Tweet) = thatTweet

  /**
   * The following methods are already implemented
   */

  def contains(tweet: Tweet): Boolean = false

  def incl(tweet: Tweet): TweetSet = new NonEmpty(tweet, new Empty, new Empty)

  def remove(tweet: Tweet): TweetSet = this

  def foreach(f: Tweet => Unit): Unit = ()
}

class NonEmpty(elem: Tweet, left: TweetSet, right: TweetSet) extends TweetSet {

  override def filterAcc(p: Tweet => Boolean, acc: TweetSet): TweetSet =
    p(elem) match {
      case true =>  left.filterAcc(p, right.filterAcc(p, acc.incl(elem)))
      case false => left.filterAcc(p, right.filterAcc(p, acc))
    }

  /**
   * the incl method returns the same Set untouched in case the given elem does already exist
   * it could have been implemented like this: this.right.union(this.left.union(modifiedSet)), as it doesn't matter if
   * the right branch is unioned first or the left branch first.
   * */
  override def union(that: TweetSet): TweetSet = this.left.union(this.right.union(that.incl(this.elem)))

  override def mostRetweeted: Tweet = influential(this.elem)
    //compare(this.left.influential(this.elem), this.right.influential(this.elem))

  /**
   *
   * returns the Tweet having a higher number of tweets and therefore considered as more influential than the other
   * */
  override def influential(mostPopular:Tweet):Tweet =
    compare(
      this.left.influential(compare(elem,mostPopular)),
      this.right.influential(compare(elem,mostPopular))
    )
    /*if (this.elem.retweets > mostPopular.retweets)
      compare(this.left.influential(elem), this.right.influential(elem))
    else
      compare(this.left.influential(mostPopular),this.right.influential(mostPopular))*/

  def compare (tweet1:Tweet, tweet2:Tweet) =
    if(tweet1.retweets > tweet2.retweets)
      tweet1
    else
      tweet2

  override def descendingByRetweet :TweetList = {
    val mostInfluential = this.mostRetweeted

    new Cons(mostInfluential,this.remove(mostInfluential).descendingByRetweet)
  }

  /**
   * The following methods are already implemented
   */

  def contains(x: Tweet): Boolean =
    if (x.text < elem.text) left.contains(x)
    else if (elem.text < x.text) right.contains(x)
    else true

  def incl(x: Tweet): TweetSet = {
    if (x.text < elem.text) new NonEmpty(elem, left.incl(x), right)
    else if (elem.text < x.text) new NonEmpty(elem, left, right.incl(x))
    else this
  }

  def remove(tw: Tweet): TweetSet =
    if (tw.text < elem.text) new NonEmpty(elem, left.remove(tw), right)
    else if (elem.text < tw.text) new NonEmpty(elem, left, right.remove(tw))
    else left.union(right)

  def foreach(f: Tweet => Unit): Unit = {
    f(elem)
    left.foreach(f)
    right.foreach(f)
  }
}

trait TweetList {
  def head: Tweet
  def tail: TweetList
  def isEmpty: Boolean
  def foreach(f: Tweet => Unit): Unit =
    if (!isEmpty) {
      f(head)
      tail.foreach(f)
    }
}

object Nil extends TweetList {
  def head = throw new java.util.NoSuchElementException("head of EmptyList")
  def tail = throw new java.util.NoSuchElementException("tail of EmptyList")
  def isEmpty = true
}

class Cons(val head: Tweet, val tail: TweetList) extends TweetList {
  def isEmpty = false
}


object GoogleVsApple {
  val google = List("android", "Android", "galaxy", "Galaxy", "nexus", "Nexus")
  val apple = List("ios", "iOS", "iphone", "iPhone", "ipad", "iPad")

  def isMatching(tweet:Tweet, referenceList:List[String]): Boolean = referenceList match {
    case scala.collection.immutable.Nil => false
    case x::scala.collection.immutable.Nil => tweet.text.contains(x)
    case x::xs =>
      tweet.text.contains(x) match {
        case false => isMatching(tweet, xs)
        case _ => true
      }
  }

  lazy val googleTweets: TweetSet = TweetReader.allTweets.filter(isMatching(_,google))
  lazy val appleTweets: TweetSet = TweetReader.allTweets.filter(isMatching(_,apple))

  /**
   * A list of all tweets mentioning a keyword from either apple or google,
   * sorted by the number of retweets.
   */
  lazy val trending: TweetList = (googleTweets.union(appleTweets)).descendingByRetweet
}

object Main extends App {
  //GoogleVsApple.googleTweets foreach println
  //GoogleVsApple.appleTweets foreach println

  //GoogleVsApple.googleTweets.descendingByRetweet foreach println

  //GoogleVsApple.appleTweets.descendingByRetweet foreach println

  // Print the trending tweets
  GoogleVsApple.trending foreach println
}
