package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {


  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }

  
  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  test("singleton sets") {
    val singletonSet_100 = singletonSet(100)
    val singletonSet_500 = singletonSet(500)

    assert(singletonSet_100(100) == singletonSet_500(500))
    assert(singletonSet_100(101) == singletonSet_500(501))

    assert(singletonSet_100(100) === true)
    assert(singletonSet_100(98) === false)
    assert(singletonSet_100(99) === false)
    assert(singletonSet_100(101) === false)
    assert(singletonSet_100(102) === false)

    assert(singletonSet_500(500) === true)
    assert(singletonSet_500(498) === false)
    assert(singletonSet_500(499) === false)
    assert(singletonSet_500(501) === false)
    assert(singletonSet_500(502) === false)
  }
  
  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   * 
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   * 
   *   val s1 = singletonSet(1)
   * 
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   * 
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   * 
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s3_ = singletonSet(3)
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   * 
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {
    
    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3". 
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("union contains all elements") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("intersect contains the intersected elements") {
    new TestSets {
      val s = intersect(s1, s2)
      assert(!contains(s, 1), "intersect 1")
      assert(!contains(s, 2), "intersect 2")
      assert(!contains(s, 3), "intersect 3")

      val sx = intersect (s2,s2)
      assert(contains(sx, 2), "intersect 2")
      assert(!contains(sx, 3), "intersect 3")
      assert(!contains(sx, 4), "intersect 4")
      assert(!contains(sx, 1), "intersect 1")

      val sy = intersect (s3,s3_)
      assert(contains(sy, 3), "intersect 3")
      assert(!contains(sy, 2), "intersect 2")
      assert(!contains(sy, 4), "intersect 4")
      assert(!contains(sy, 5), "intersect 5")
    }
  }

  test("diff contains all the elements in the first only") {
    new TestSets {
      val s = diff(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(!contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("filter returns a set that contains only the elements that satisfies the given predicate") {
    new TestSets {
      val s_1 = filter(s1, x => x==1)
      assert(contains(s_1, 1), "filter 1")
      assert(!contains(s_1, 2), "filter 2_1")
      assert(!contains(s_1, 3), "filter 3")

      val s_2 = filter(s1, x => x==2)
      assert(!contains(s_2, 1), "filter 1")
      assert(!contains(s_2, 2), "filter 2_2")
      assert(!contains(s_2, 3), "filter 3")


      val s_3 = filter(x => 5 < x && x < 10, x => x==2)
      assert(!contains(s_3, 1), "filter 1")
      assert(!contains(s_3, 2), "filter 1")
      assert(!contains(s_3, 6), "filter 1")
      assert(!contains(s_3, 2), "filter 2_3")
      assert(!contains(s_3, 7), "filter 3")
      assert(!contains(s_3, 8), "filter 3")
      assert(!contains(s_3, 9), "filter 3")

      val s_4 = filter(x => 5 < x && x < 10, x => x==8)
      assert(!contains(s_4, 5), "filter 1")
      assert(!contains(s_4, 6), "filter 6")
      assert(!contains(s_4, 7), "filter 7")
      assert(contains(s_4, 8), "filter 8")
      assert(!contains(s_4, 9), "filter 9")
    }
  }

  test("forall and s=") {
    new TestSets {
      val f =
      forall(x => -1000 < x && x < 1000, y => y > 9)

      assert(f === false)

      val f1 =
        forall(x => -1000 < x && x < 1000, y => y > 0)

      assert(f1 === false)

      val f2 =
        forall(x => -1000 < x && x < 1000, y => -1000 < y && y < 1000)

      assert(f2 === true)

      val f3 =
        forall(x => -30 < x && x < 30, y => -1000 < y && y < 1000)

      assert(f3 === true)

      val f4 =
        forall(x => -30 < x && x < 30, y => y > 3)

      assert(f4 === false)

      val f5 =
        forall(x => x == 1000, y => y > 3)

      assert(f5 === true)

      val f6 =
        forall(x => x == -1000, y => y > 3)

      assert(f6 === false)

      val f7 =
        forall(x => x == -1000, y => y < -3)

      assert(f7 === true)

      val f8 =
        forall(x => x == -999, y => y == -998)

      assert(f8 === false)

      val f9 =
        forall(x => x == -999, y => y == -999)

      assert(f9 === true)


      val f10 =
        forall(x => x == 0, y => y >= -999)

      assert(f10 === true)

    }
  }

  test("exists") {
    //*************************
    //not all elements of the given set satisfies the given predicate
    val f1 = forall (x => -30 < x && x < 30 , y => y ==0)
    assert(f1 === false)

    //yet, there exists one element in the set that satisfies it
    val f2 = exists (x => -30 < x && x < 30 , y => y ==0)
    assert(f2 === true)

    //*************************
    //not all elements of the given set satisfies the given predicate
    val f3 = forall (x => -30 < x && x < 30 , y => y ==0)
    assert(f3 === false)

    //yet there exists some elements in the given set satisfies the given predicate
    val f4 = exists (x => -30 < x && x < 30 , y => y >= 0)
    assert(f4 === true)

    //*************************
    val f5_ = forall (x => -30 < x && x < 30 , y => -30 < y && y < 30)
    assert(f5_ === true)

    val f5__ = exists (x => -30 < x && x < 30 , y => -30 < y && y < 30)
    assert(f5__ === true)

    //*************************
    // not all elements satisfy the condition implied by the predicate
    val f6_ = forall (x => -30 < x && x < 30 , y => y > 30)
    assert(f6_ === false)

    // no single element satisfy the condition implied by the predicate
    val f6__ = exists (x => -30 < x && x < 30 , y => y > 30)
    assert(f6__ === false)

    //*************************
    val f5 = exists (x => -30 < x && x < 30 , y => y > 30)
    assert(f5 === false)

    val f6 = exists (x => -30 < x && x < 30 , y => y == 29)
    assert(f6 === true)
  }

  test("map") {
    val modifiedSet_1  = map(x => x == 0, y => y +10)

    assert(contains(modifiedSet_1, 10),"10 is correct")
    assert(!contains(modifiedSet_1, 0), "0 is not correct")

    val modifiedSet_2  = map(x => x > 0 && x <100, y => y +10)

    assert(!contains(modifiedSet_2, 0), "0 is not correct too")
    assert(!contains(modifiedSet_2, 10), "10 is correct too")
    assert(contains(modifiedSet_2, 20), "20 is correct")
    assert(contains(modifiedSet_2, 30), "30 is correct")
    assert(contains(modifiedSet_2, 40), "40 is correct")
    assert(contains(modifiedSet_2, 50), "50 is correct")
    assert(contains(modifiedSet_2, 60), "60 is correct")
    assert(contains(modifiedSet_2, 70), "70 is correct")
    assert(contains(modifiedSet_2, 80), "80 is correct")
    assert(contains(modifiedSet_2, 90), "90 is correct")
    assert(contains(modifiedSet_2, 100), "100 is correct")
    assert(contains(modifiedSet_2, 109), "110 is correct")
    assert(!contains(modifiedSet_2, 110), "110 is correct")
    assert(!contains(modifiedSet_2, 120), "120 is not correct")


    val modifiedSet_3  = map(x => x == 0, y => y +10)

    assert(contains(modifiedSet_3, 10), "10 is correct too too")
    assert(!contains(modifiedSet_3, 9), "10 is not correct too too")
    assert(!contains(modifiedSet_3, 0), "10 is not correct too too")


    val modifiedSet_4  = map(x => x > 0 && x <100, y => y)

    assert(!contains(modifiedSet_4, 0), "0 is correct")

    assert(contains(modifiedSet_4, 10), "10 is correct too too too")

    assert(contains(modifiedSet_4, 99), "10 is correct too too too")

    assert(!contains(modifiedSet_4, 100), "100 is correct")

    assert(!contains(modifiedSet_4, 110), "110 is not correct")

  }

}
