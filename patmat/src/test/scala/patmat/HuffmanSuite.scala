package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
    val t2 = Fork(
                  Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5),
                  Leaf('d',4), List('a','b','d'), 9)
    val x = 3
  }

  /*****************************************************************************************/
  test("weight of a larger tree-t1") {
    new TestTrees {
      assert(weight(t1) === 5)
      x
    }
  }

  test("weight of a larger tree-t2") {
    new TestTrees {
      assert(weight(t2) === 9)
    }
  }

  /*****************************************************************************************/
  test("chars of a larger tree-t1") {
    new TestTrees {
      assert(chars(t1) === List('a','b'))
    }
  }

  test("chars of a larger tree-t2") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
    }
  }

  /*****************************************************************************************/
  test("make code tree-1") {
    new TestTrees {
      assert(makeCodeTree(Leaf('a',2), Leaf('b',3)) === t1)
    }
  }

  test("make code tree-2") {
    new TestTrees {
      assert(makeCodeTree(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4)) === t2)
    }
  }

  /*****************************************************************************************/
  test("times-1") {
    new TestTrees {
      assert(times(List('a', 'b', 'a')) === List(('a', 2), ('b', 1)))
    }
  }

  test("times-2") {
    new TestTrees {
      assert(times(List('a', 'b', 'a', 'c', 'd' , 'e')) === List(('b', 1), ('a', 2),('c', 1), ('d', 1), ('e', 1)).reverse)
    }
  }

  test("times-3") {
    new TestTrees {
      assert(times(List('a', 'b', 'a', 'a', 'a', 'b', 'c', 'a')) === List(('a', 5), ('c', 1), ('b', 2)))
    }
  }

  test("times-4") {
    new TestTrees {
      assert(times(List('a', 'a', 'a', 'a')) === List(('a', 4)))
    }
  }

  test("times-5") {
    new TestTrees {
      assert(times(List('a', 'b', 'c')) === List(('a', 1), ('b', 1), ('c', 1)).reverse)
    }
  }

  test("times-6") {
    new TestTrees {
      assert(times(List('a', 'd', 'd', 'd', 'x', 'a')) === List(('a', 2), ('x', 1),('d', 3)))
    }
  }
  /*****************************************************************************************/

  test("makeOrderedLeafList-1") {
    new TestTrees {
      assert(makeOrderedLeafList( List(('a', 1), ('b', 1), ('c', 1))) === List(Leaf('a', 1), Leaf('b', 1), Leaf('c', 1)))
    }
  }

  test("makeOrderedLeafList-2") {
    new TestTrees {
      assert(makeOrderedLeafList( List(('a', 2), ('x', 1),('d', 3))) === List(Leaf('x', 1), Leaf('a', 2),Leaf('d', 3)))
    }
  }

  test("makeOrderedLeafList-3") {
    new TestTrees {
      assert(makeOrderedLeafList( List(('a', 2), ('x', 1),('d', 3), ('f', 3), ('r', 0))) === List(Leaf('r', 0), Leaf('x', 1), Leaf('a', 2),Leaf('d', 3), Leaf('f', 3)))
    }
  }

  test("makeOrderedLeafList-4") {
    new TestTrees {
      assert(makeOrderedLeafList( List(('a', 13), ('b', 16), ('x', 1), ('s', 29), ('d', 1), ('g', 123))) ===
        List(Leaf('x', 1), Leaf('d', 1), Leaf('a', 13), Leaf('b', 16), Leaf('s', 29), Leaf('g', 123)))
    }
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }

  /*****************************************************************************************/

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  /*****************************************************************************************/
  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    //his test
    //assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))

    //my test
    assert(combine(leaflist) === List(Fork(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3),Leaf('x',4),List('e', 't', 'x'),7)))
  }

  test("combine of leaf and forks list") {
    val hybridList = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4), Fork(Leaf('h', 4),Leaf('z', 8), List('h','z'),12))

    //my test
    assert(combine(hybridList) === //List(Fork(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3),Leaf('x',4),List('e', 't', 'x'),7)))
    List(
      Fork(
        Fork(
          Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3),Leaf('x',4),List('e', 't', 'x'),7),
        Fork(Leaf('h',4),Leaf('z',8),List('h', 'z'),12),List('e', 't', 'x', 'h', 'z'),19) )
    )
  }

  /*****************************************************************************************/
  test("until-1") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    //assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
    val xx = List(Fork(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3),Leaf('x',4),List('e', 't', 'x'),7))

    assert(until(singleton,combine)(leaflist) === xx)
  }
  /*****************************************************************************************/

  test("createCode-1") {
    //this is an example of a huffman tree

    val testString1 = List('t','h','i','s',' ', 'i', 's', ' ', 'a', 'n', ' ', 'e', 'x', 'a', 'm', 'p', 'l', 'e', ' ', 'o', 'f', ' ', 'a', ' ', 'h', 'u', 'f', 'f', 'm', 'a', 'n', ' ', 't', 'r', 'e', 'e')

    // works in case the <= is made in the combine func
    val expectedCodeTree_output =
      Fork(
        Fork(
           Fork(Leaf('e',4),Leaf('a',4),List('e', 'a'),8)
          ,Fork(
             Fork(Leaf('t',2),Leaf('n',2),List('t', 'n'),4)
            ,Fork(Leaf('m',2),Leaf('h',2),List('m', 'h'),4)
            ,List('t', 'n', 'm', 'h'),8)
          ,List('e', 'a', 't', 'n', 'm', 'h'),16),
        Fork(
          Fork(
             Fork(Leaf('s',2),Leaf('i',2),List('s', 'i'),4)
            ,Fork(
               Fork(Leaf('r',1),Leaf('u',1),List('r', 'u'),2)
              ,Fork(Leaf('o',1),Leaf('l',1),List('o', 'l'),2)
              ,List('r', 'u', 'o', 'l'),4)
            ,List('s', 'i', 'r', 'u', 'o', 'l'),8)
          ,Fork(
            Fork(
              Fork(Leaf('p',1),Leaf('x',1),List('p', 'x'),2)
              ,Leaf('f',3)
              ,List('p', 'x', 'f'),5)
            ,Leaf(' ',7)
            ,List('p', 'x', 'f', ' '),12)
          ,List('s', 'i', 'r', 'u', 'o','l', 'p', 'x', 'f', ' '),20)
        ,List('e', 'a', 't', 'n', 'm', 'h', 's', 'i', 'r', 'u', 'o', 'l', 'p','x', 'f',' '),36)


    // works in case the < is made in the combine func
    val expectedOutput_2 =
      Fork(
        Fork(

          Fork(Leaf('e',4),Leaf('a',4),List('e', 'a'),8)
         ,Fork(
              Fork(
                  Fork(Leaf('r',1),Leaf('u',1),List('r', 'u'),2)
                 ,Leaf('t',2)
                 ,List('r', 'u', 't'),4),
              Fork(
                Fork(Leaf('p',1),Leaf('x',1),List('p', 'x'),2)
               ,Fork(Leaf('o',1),Leaf('l',1),List('o', 'l'),2)
               ,List('p', 'x', 'o', 'l'),4)
             ,List('r', 'u', 't', 'p', 'x', 'o', 'l'),8)
         ,List('e', 'a', 'r', 'u', 't', 'p', 'x', 'o', 'l'),16)

        ,Fork(Fork(Fork(Leaf('h',2),Leaf('s',2),List('h', 's'),4),Fork(Leaf('n',2),Leaf('m',2),List('n', 'm'),4),List('h', 's', 'n', 'm'),8)
          ,Fork(Fork(Leaf('i',2),Leaf('f',3),List('i', 'f'),5),Leaf(' ',7),List('i', 'f',' '),12),List('h', 's','n', 'm', 'i', 'f',' ' ),20),List('e', 'a', 'r', 'u', 't', 'p', 'x', 'o', 'l', 'h', 's','n', 'm', 'i', 'f',' '),36)

    assert(createCodeTree(testString1) === expectedOutput_2)
  }

  test("decode-1") {
    new TestTrees {

      val sampleCode =
        Fork(
          Fork(
            Fork(Leaf('e',4),Leaf('a',4),List('e', 'a'),8)
           ,Fork(
              Fork(Leaf('t',2),Leaf('n',2),List('t', 'n'),4)
              ,Fork(Leaf('m',2),Leaf('h',2),List('m', 'h'),4)
              ,List('t', 'n', 'm', 'h'),8)
            ,List('e', 'a', 't', 'n', 'm', 'h'),16),
          Fork(
            Fork(
              Fork(Leaf('s',2),Leaf('i',2),List('s', 'i'),4)
             ,Fork(
                Fork(Leaf('r',1),Leaf('u',1),List('r', 'u'),2)
                ,Fork(Leaf('o',1),Leaf('l',1),List('o', 'l'),2)
                ,List('r', 'u', 'o', 'l'),4)
              ,List('s', 'i', 'r', 'u', 'o', 'l'),8)
            ,Fork(
              Fork(
                Fork(Leaf('p',1),Leaf('x',1),List('p', 'x'),2)
                ,Leaf('f',3)
                ,List('p', 'x', 'f'),5)
              ,Leaf(' ',7)
              ,List('p', 'x', 'f', ' '),12)
            ,List('s', 'i', 'r', 'u', 'o','l', 'p', 'x', 'f', ' '),20)
          ,List('e', 'a', 't', 'n', 'm', 'h', 's', 'i', 'r', 'u', 'o', 'l', 'p','x', 'f',' '),36)

      val sampleSecret: List[Bit] = List(0,1,0,0,0,0,1,1,0,1,1,0,1,0,1,0,0,0)

      assert(decodedSecret === List('h','u','f','f','m','a','n','e','s','t','c','o','o','l'))
    }
  }

  test("encode-1") {
    new TestTrees {
      assert(encode(t1) (List('a','b')) === List (0,1))
    }
  }

  test("encode-2") {
    new TestTrees {
      assert(encode(t2) (List('a','b','d')) === List (0,0,0,1,1))
    }
  }

  /**
   * This one works fine because the encoded text contains no white spaces
   * */
  test("encode-3") {
    new TestTrees {
      assert(encode(frenchCode)("huffmanestcool".toList) === frenchSecret)
    }
  }

  /**
   * This one works fine because the encoded text contains no white spaces
   * */
  test("quick-encode-1") {
    new TestTrees {
      assert(quickEncode(frenchCode)("huffmanestcool".toList) === frenchSecret)
    }
  }

  /**
   * This one works fine because the encoded text contains no white spaces
   * */
  test("quick-encode-2") {
    new TestTrees {
      assert(quickEncode(t2) (List('a','b','d')) === List (0,0,0,1,1))
    }
  }

  /**
   * This one works fine because the encoded text contains no white spaces
   * */
  test("decode and encode a very short text should be identity") {
    new TestTrees {
       assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  /**
   * This one works fine because the encoded text contains no white spaces
   * There is a problem with the space character... if the encoded text contains a space, only the characters after the
   * last space get decoded correctly to the original text, while all preceding characters are kicked out... it is either
   * a problem in the encoding or in the decoding .. one of them might be mistakenly dropping the space chars!
   * */
  test("decode and encode on a long text should be identity") {
    // THERE IS A PROBLEM WITH THE SPACE... IF THE ENCODED TEXT CONTAINS A SPACE, ONLY THE CHARACTERS AFTER THE LAST
    // SPACE GETS DECODED CORRECTLY, WHILE ALL THE PRECEDING CHARACTERS ARE KICKED OUT... IT IS EITHER A PROBLEM IN THE
    // ENCODING OR IN THE DECODING.. ONE OF THEM IS DROPPING THE SPACE CHARS
    new TestTrees {
      assert(decode(frenchCode,encode(frenchCode)(List('n','a','d','e','r', ' ', 'l','o','v','e','s', ' ' , 'n','o','h','a')))
        === List('n','a','d','e','r', 'l','o','v','e','s', ' ', 'n','o','h','a'))
    }
  }

  /**
   * This one works fine because the encoded text contains no white spaces
   * */
  test("decode-quick-encode-1") {
    new TestTrees {
      assert(decode(t1, quickEncode(t1)("ab".toList)) === "ab".toList)
    }
  }

  /**
   * This one works fine because the encoded text contains no white spaces
   * */
  test("decode-quick-encode-2") {
    new TestTrees {
      assert(decode(frenchCode,quickEncode(frenchCode)(List('n','a','d','e','r','l','o','v','e','s','n','o','h','a')))
        === List('n','a','d','e','r','l','o','v','e','s','n','o','h','a'))
    }
  }

  /**
   * This one works fine because the encoded text contains no white spaces
   * There is a problem with the space character... if the encoded text contains a space, only the characters after the
   * last space get decoded correctly to the original text, while all preceding characters are kicked out... it is either
   * a problem in the encoding or in the decoding .. one of them might be mistakenly dropping the space chars!
   * */
  test("decode-quick-encode-3") {
    // THERE IS A PROBLEM WITH THE SPACE... IF THE ENCODED TEXT CONTAINS A SPACE, ONLY THE CHARACTERS AFTER THE LAST
    // SPACE GETS DECODED CORRECTLY, WHILE ALL THE PRECEDING CHARACTERS ARE KICKED OUT... IT IS EITHER A PROBLEM IN THE
    // ENCODING OR IN THE DECODING.. ONE OF THEM IS DROPPING THE SPACE CHARS
    new TestTrees {
      assert(decode(frenchCode,quickEncode(frenchCode)(List('n','a','d','e','r',' ', 'l','o','v','e','s', ' ', 'n','o','h','a')))
        === List('n','a','d','e','r',' ', 'l','o','v','e','s', ' ', 'n','o','h','a'))
    }
  }

  /**
   * encode and quick encode
   * */
  test("performance-1") {
    new TestTrees {

      //5,4135815 ns nano seconds = 5.4 milli seconds
      val time1 = System.nanoTime()
      val result1 = decode(frenchCode,encode(frenchCode)(List('n','a','d','e','r','l','o','v','e','s','n','o','h','a')))
      val time2 = System.nanoTime()
      println("encode for the french code took : " + (time2 - time1) + "ns")

      //9,275346 ns nano second = 9.2 milli seconds
      val time3 = System.nanoTime()
      val result2 = decode(frenchCode,quickEncode(frenchCode)(List('n','a','d','e','r','l','o','v','e','s','n','o','h','a')))
      val time4 = System.nanoTime()
      println("quick encode for the french code took : " + (time4 - time3) + "ns")

      println(result1.toString)

      assert (result1 === result2)
    }
  }

  /**
   * encode and quick encode
   * */
  test("performance-2") {
    new TestTrees {

      val text = "forrasetofsymbolswithauniformprobabilitydistributionandanumberofmemberswhichisapoweroftwoHuffmancodingisequivalent ,x, ".toList// to simple binary block encoding, e.g., ASCII coding. Huffman coding is such a widespread method for creating prefix codes that the term Huffman code is widely used as a synonym for prefix code even when such a code is not produced by Huffman's algorithm.".toList)
      //104,752231 ns = 104 milli seconds
      val time1 = System.nanoTime()
      val encodedText = encode(frenchCode)(text)
      val time2 = System.nanoTime()
      println("encode for the french code took : " + (time2 - time1) + "ns")
     // assert (text === decode(frenchCode,encodedText))

      //4,539797 ns 4 milli seconds
      val time3 = System.nanoTime()
      //val quickEncodedText = quickEncode(frenchCode)(text)
      val quickEncodedText = quickEncode(frenchCode)(text)
      val time4 = System.nanoTime()
      println("quick encode for the french code took : " + (time4 - time3) + "ns")
      assert (text === decode(frenchCode,quickEncodedText))

     // assert ( encode(frenchCode)(text) ===  quickEncode(frenchCode)(text))

      /*val time5 = System.nanoTime()
      //val quickEncodedText = quickEncode(frenchCode)(text)
      val quickEncodedText2 = encode(frenchCode)(text)
      val time6 = System.nanoTime()
      println("quick encode for the french code took : " + (time6 - time5) + "ns")

      val time7 = System.nanoTime()
      //val quickEncodedText = quickEncode(frenchCode)(text)
      val quickEncodedText3 = encode(frenchCode)(text)
      val time8 = System.nanoTime()
      println("quick encode for the french code took : " + (time8 - time7) + "ns")

      val time9 = System.nanoTime()
      //val quickEncodedText = quickEncode(frenchCode)(text)
      val quickEncodedText4 = encode(frenchCode)(text)
      val time10 = System.nanoTime()
      println("quick encode for the french code took : " + (time10 - time9) + "ns")

      assert (text === decode(frenchCode,quickEncodedText))*/


      /* val result1 = decode(frenchCode,encode(frenchCode)("For a set of symbols with a uniform probability distribution and a number of members which is a power of two, Huffman coding is equivalent to simple binary block encoding, e.g., ASCII coding. Huffman coding is such a widespread method for creating prefix codes that the term Huffman code is widely used as a synonym for prefix code even when such a code is not produced by Huffman's algorithm.".toList))
       val time2 = System.nanoTime()
       println("encode for the french code took : " + (time2 - time1) + "ns")

       //14545924 ns = 14 milli seconds
       val time3 = System.nanoTime()
       val result2 = decode(frenchCode,quickEncode(frenchCode)("For a set of symbols with a uniform probability distribution and a number of members which is a power of two, Huffman coding is equivalent to simple binary block encoding, e.g., ASCII coding. Huffman coding is such a widespread method for creating prefix codes that the term Huffman code is widely used as a synonym for prefix code even when such a code is not produced by Huffman's algorithm.".toList))
       val time4 = System.nanoTime()
       println("quick encode for the french code took : " + (time4 - time3) + "ns")

       println(result1.toString)
       //println(result2.)
       assert (result1 === result2)*/
    }
  }
}

// encode takes less time in subsequent times
// quick encode and encode drops the first character sometimes --> done
// spaces are not correctly handled in both encode and quick encode
// encode and quickEncode returns a zero sequence if the character ',' appears
