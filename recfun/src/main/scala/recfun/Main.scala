package recfun

import scala.annotation.tailrec

object Main {
  def main(args: Array[String]) {
    /*println("Pascal's Triangle")
    for (row <- 0 to 20) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }*/

    println(countChange(8,List(1,3,5)))

  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c == r || c == 0 || r == 0) 1
    else pascal(c-1,r-1) + pascal(c,r-1)
  }

  /**
   * Exercise 2
   */

  def balance(chars: List[Char]): Boolean = {

    @tailrec
    def balance(chars: List[Char], openCount: Int): Boolean = {
      chars match {
        case Nil => openCount==0
        case x::xs => x match {
          case '(' => balance(xs, openCount + 1)
          case ')' if openCount > 0 => balance(xs, openCount - 1)
          case ')' => false
          case _ => balance(xs, openCount)
        }
      }
    }

    if (chars.isEmpty)
      true
    else balance(chars, 0)
  }

  /**
   * Exercise 3
   */

  /**
   * Trial number 4
   * think about the order of complexity of this algorithm
   * */
  def countChange(money:Int, coins:List[Int]): Int = {
    money match {
      case 0 => 1
      case m if m < 0 => 0
      case _ if coins.isEmpty && money >= 1 => 0
      case _ => countChange(money, coins.tail) + countChange(money - coins.head, coins)
    }
  }


  // Trial 1
  /* def countChange(money: Int, denominations: List[Int]) = {

    calculateRemainder(money,denominations,0)

    def calculateRemainder(upper:Int, denominations: List[Int], optionsCount: Int): Int = {

     var slicedDenominations = denominations.slice(0, 1)

      var count = optionsCount

     val lowerBound = denominations.reverse.head

     upper < lowerBound match {
       case true => optionsCount
       case false => {
         upper % lowerBound match {
           case 0 => count = optionsCount + 1
           case remainder if remainder > 0 => calculateRemainder(remainder, denominations,optionsCount)
         }
       }
     }

      //could have been done like this (0 /: denominations) (_ + _)
      if (denominations.length > 1) {

        val foldedDenominations = denominations.foldLeft(0)(_ + _)

        upper < foldedDenominations match {
          case true => 0
          case false => upper % foldedDenominations match {
            case 0 => count = optionsCount + 1
            case remainder if remainder > 0 => calculateRemainder(remainder, denominations,count)
            case _ => println("shouldn't happen")
              0
          }
        }
      }
      count
    }

    //calculate here the different combinations that can be sent to the calculateRemainder function, in a way that can
    // cover all the different combinations from the coins (1+3, 1+5, 1+3+5, etc....)

    /*denominations foreach {
      money % _ match {
        case 0 => optionsCount += 1
      }
    }*/

   /* var slicedDenominations = denominations.slice(0, 1)
    optionsCount += calculateRemainder(money, slicedDenominations,0)

    slicedDenominations = denominations.slice(0, 2)
    optionsCount += calculateRemainder(money, slicedDenominations,optionsCount)*/

   /* slicedDenominations = denominations.slice(0, 3)
    optionsCount += calculateRemainder(money, slicedDenominations)

    slicedDenominations = denominations.slice(0, 4)
    optionsCount += calculateRemainder(money, slicedDenominations)*/

    //slicedDenominations = denominations.slice(1, 2)
    //optionsCount += calculateRemainder(money, slicedDenominations)

    /*slicedDenominations = denominations.slice(1, 3)
    optionsCount += calculateRemainder(money, slicedDenominations)

    slicedDenominations = denominations.slice(1, 4)
    optionsCount += calculateRemainder(money, slicedDenominations)

    slicedDenominations = denominations.slice(2, 3)
    optionsCount += calculateRemainder(money, slicedDenominations)

    slicedDenominations = denominations.slice(2, 4)
    optionsCount += calculateRemainder(money, slicedDenominations)*/

    if (denominations.isEmpty)
      0
    if (money == 0)
      0


   /* calculateRemainder(money, denominations.head) match {
      case 0 => optionsCount+=1

      case remainingMoney if remainingMoney > 0 =>
        // call the calculateRemainder functions with the correct combinations
        calculateRemainder(remainingMoney,denominations.head) match {
          case 1 => calculateRemainder()

        }
    }

    def calculateRemainder(upper: Int, lower: Int) ={
      upper % lower match {
        case 0 => 1
        case x if x<0 => -1
        case x if x>0 => 1
      }
    }

    def calculateRemainder(money: Int, baseCoin:Int, remainingCoins:List[Int]): Int ={
      var optionsCount = 0

      money % baseCoin match {
        case 0 => optionsCount += 1
                  optionsCount += calculateRemainder(money,baseCoin+remainingCoins.head, remainingCoins)
        case x if x > 0 => optionsCount += calculateRemainder(money,x,lower)
        case _ => 0
      }

      optionsCount
    }*/

    // example of 8 and 1,3,5

    //iteration 1:

    // 1+3+5 = 9 >8 =>go out

    // 1+3 =4 < 8, then continue with the 4
      // 4+5 > 8 go out
      // 4+3 =7 < 8 then continue with the 7
         // 7 + 1 = 8 OK        (3+1+3+1)
         // 7 + 3 = 10 > 8 go out
         // 7 + 5 = 12 > 8 go out
      // 4+1 =5 < 8 then continue with the 5
         //5+1=6 < 8 then continue with the 6
            // 6+1=7 < 8 then continue with the 7
              //7 +1 = 8 OK     (1+3+1+1+1+1)
            // 6+3 >8 NO
            // 6+5 >8 NO

         //5+3=8 OK             (5+3)
         //5+5=10 >8 go out

    //1+5=6 < 8 then continue with the 6
      //6+1 =7 <8 then continue with 7
        //7+1  OK               (1+5+1+1)
      //6+3 =9 >8, then go out
      //6+5 =11 >8, then go out


    /*val sum2 = denominations.foldLeft(0,sum(0,_))

    def count (baseValue: Int, remainingCoins: List[Int]):Int ={
      var initialValue = baseValue
      initialValue+= remainingCoins.head
    }*/

    //money is 4
    //coins are 1, 2

    // the combination that you use it to get the new value after the substraction don't use it again in the division

    /*for (coin <- denominations)
      coin

    (4-1)%2 //no
    (4-1)%1 //yes 1+1+1+1
    (4-2)%1 //yes 1+1 +2
    (4-2)%2 //yes 2 +2
    (4-2-1)%1 // yes 1+2+1
    (4-2-1)%2 // no
    (4-2-1)%1+2 // no

    4%1 =>yes
    4%2 =>yes
    4%(1+2)=> remainder 1
    1%1=yes ==> 1+2+1


    // 8 and 1,2
    //1+1+1+1+1+1+1+1 - 2+2+2+2 - 1+1+2+2+2 - 1+1+1+1+2+2 - 1+1+1+1+1+1+2
    8%1 //YES
    8%2 //YES

    // Work on 7 (8-1)
    //7%1//YES
    7%2//NO
    7%3//NO

    // work on 7 -1
    6%2//YES 2 + 2 + 2 + 1 + 1
    6%3//YES

    // work on 7 -2

    // work on 7 -3

    // Work on 6 (8-2)
    6%1//YES
    //6%2//2+2+2+2
    6%3//YES 1+2 + 1+2 +2

    // Work on 5 (8-1-2)
    5%1 //YES ==> 1+1+1+1+1 + 2+1
    5%2 //no
    //5%3 //no



  //(8-3-1)%1 // YES 2+1+1 + 1+1+1+1
    (8-3-1)%2 // YES 1+2+1+2+2
    (8-3-1)%3 // no

    (8-3-2)%1 //YES 2+1 +2 + 1+1+1
  //  (8-3-2)%2 // no
    (8-3-2)%3 // no

    (8-3-3)%1 //YES 2+1 + 2+1 +1+1
    (8-3-3)%2 //YES 2+1 + 2+1 + 2
    //(8-3-3)%3 //NO

   // (8-3-3-1)%1
    (8-3-3-1)%2//NO
    (8-3-3-1)%3//NO

    // one more option here 2+1 +2+1 +2 we can ignore it
    (8-3-3-2)%1
    //(8-3-3-2)%2
    (8-3-3-2)%3

    (8-3-3-3)%1//NO
    (8-3-3-3)%2//NO
    //(8-3-3-3)%3


    coins match {
      case Nil => 0
      case x::xs => countChange()

    }

    def divide (upper: Int, lower: Int): Boolean ={
      val division = upper % lower

      if division == 0
        true
    }

*/
    //iterate on the list of coins
    //divide the money by the first coin and get the remainder
    //if remainder is zer, then we have one way
    //call the same function with the rest of the list
  }*/

  // Trial 2
  /*
  def countChange(money:Int, coins:List[Int]) ={

    //@tailrec
    def countChange(money:Int, coins:List[Int], changeCount:Int):Int = {
      var openCount = changeCount
      if (coins.isEmpty)
        openCount
      else {
        openCount = inspect(money, coins,changeCount)
        countChange(money, coins.tail,openCount)
      }

    }

    /**
     * takes a number and inspects the input list to detect all the possible combinations originating from the head of the List
     * which makes us reach this target number at the end.
     *
     * each inspected combination should consider the head of the list, so as to ensure that no possible combination is
     * dropped, since the next time this function is called, it will be called with the head of the list omitted.
     *
     * first checks the remainder of the division of the target value and every combination in the remaining coin where
     * the head of the list is involved. example: if money is 8 and remainingCoins is List (1,2,3,4)... then the options
     * that are going to be considered are
     * Iteration1: 1, 1+2, 1+3, 1+4, 1+2+3, 1+2+4, 1+3+4, 1+2+3+4 ==> 1+1+1+1+1+1+1+1 and 3+1+3+1
     * Iteration2: 2, 2+3, 2+4, 2+3+4 ==> 2+2+2+2
     * Iteration3: 3, 3 and 3+4 ==> no options
     * Iteration4: 4, 4 ==> 4+4
     *
     * in every recursive call to this function the target value gets changed while the list of remaining coins remains
     * the same, which means that the same options get applied on the new target value being passed. typically,
     * a recursive call to this function happens with the remainder value... which should be inspected against the same
     * coin options
     * */
    def inspect(targetValue: Int, remainingCoins:List[Int], changes:Int):Int ={ //1,2,3,4
      var count = changes

      val splittedLists = remainingCoins.splitAt(1)

      val referenceCoin = splittedLists._1 //1
      val restOfCoins = splittedLists._2 // 2,3,4


      //covered the case of 1
      referenceCoin.foldLeft(0)(_ + _) match {
        case sum if sum < targetValue => targetValue % sum match {
          case 0 => count+=1
          case remainder if remainder > 0 => inspect(remainder,remainingCoins,count)
        }
        case sum if sum == targetValue => count+=1
        case _ => count
      }

      //covered the following cases 1+2 , 1+3 , 1+4
      restOfCoins.foreach{
        /*case x if (x < targetValue) => targetValue % x match {
          case 0 => count+=1
          case remainder if remainder > 0 => count = inspect(remainder,remainingCoins,count)
        }*/
        case x if (x+referenceCoin.head < targetValue) => targetValue % x+referenceCoin.head match {
          case 0 => count+=1
          case remainder if remainder > 0 => count = inspect(remainder,remainingCoins,count)
        }

        case x if x == targetValue => count+=1
        case _ => count
      }

      // covered the case of 2+3+4
      if (restOfCoins.length > 1) {
        remainingCoins.foldLeft(0)(_ + _) match {
          case sum if (sum < targetValue) => targetValue % sum match {
            case 0 => count += 1
            case remainder if remainder > 0 => count = inspect(remainder, remainingCoins, count)
          }

          case sum if sum == targetValue => count += 1

          case _ => count
        }
      }

      // remains the following options 1+2+3, 1+3+4, 1+2+4, 3+4

      // divide by the reference coin //1
      // divide by the fold //1+2+3+4
      // divide by the modified map // 1+2, 1+3, 1+4
      // divide by the folding of the filtered list where only one item of it is removed //

      //val filteredList = remainingCoins.filter(_)

      count
    }

    countChange (money,coins,0)
  }
  */

  // Trial 3
  /*def countChange(money:Int, coins:List[Int]): Int = {

    def countChange(money: Int, coins: List[Int], changeCount: Int): Int = {
      var combinationsCount = changeCount
      coins match {
        case Nil => combinationsCount
        // will be called three times, once with 1 as the reference coin, once with 3, and once with 5 and with all the
        // three times all other coins are being passed
        case x :: xs => combinationsCount += subtract(money, x, coins,combinationsCount)
      }

      def subtract (amount: Int, value: Int): Int= {
        (amount - value) match {
          case result if result > 0 => subtract (result, value)
          case result if result < 0 => 0
          case 0 => 1 //result = 1
        }
      }

      if (! coins.isEmpty)
        combinationsCount += countChange(money, coins.tail, combinationsCount)

      combinationsCount
    }

    // first call with the 8 as the remaining money and the 1,2,3 coins
    //def findChangeCount(remainingMoney: Int, referenceCoin: Int, coins: List[Int], count: Int): Int = {
      var changes = count

      //C[8-1]

      (remainingMoney - referenceCoin) match {
        case result if result > 0 => changes += findChangeCount (result, referenceCoin, coins,changes)
        case result if result < 0 => 0
        case 0 => changes +=1
      }

      //C[8-2]
      //C[8-3]

      //var changes = count
      if (remainingMoney ==0)
        0
      else if(remainingMoney < 0)
        0

      findChangeCount(remainingMoney-coin, coins)

      //else {
        // changes =

        //coins.foldLeft(count) {
          (acc, coin) => 1 + acc + findChangeCount(remainingMoney - coin, coins,acc)
        }//

        /*case x => if (remainingMoney > 0) 1 + findChangeCount(remainingMoney - x, coins,changes)
            case x if remainingMoney == 0 => 1
            case _ => 0*/
        //}


        //coins.foreach {
        //  case x => if (remainingMoney > 0) changes = 1 + findChangeCount(remainingMoney - x, coins,changes)
        //  case x if (remainingMoney == 0) => changes += 1
        //  case _ => 0
        //}
      //}

    }//


    countChange(money, coins, 0)
  }*/
}
